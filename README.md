# Suitmedia Internship Frontend

This repository is for the Frontend Internship Application at Suitmedia.

## Short Description
- Stack: ReactJS + Vite + Typescript
- Responsive: YES
- CSS Framework: Tailwind CSS

## How to install
1. Clone this repository using
```console 
git clone https://gitlab.com/hadhaz/suitmedia-internship-frontend
```
2. Enter the folder
```console
cd suitmedia-internship-frontend
```
3. Install Dependency
```console
yarn
```
4. Run the Development Server
```console
yarn dev
```

## Deployment
You can check the deployment to this [link](https://hadhaz-suitmedia.vercel.app/)

## Preview
![](https://gitlab.com/hadhaz/suitmedia-internship-frontend/-/raw/main/repository/screencapture-localhost-5173-2023-07-11-12_53_08.png)
