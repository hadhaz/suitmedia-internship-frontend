import Navbar from "./component/Navbar";
import HomeSlider from "./component/HomeSlider";
import CompanyValues from "./component/CompanyValueList";
import ContactForm from "./component/ContactForm";
import Footer from "./component/Footer";

function App() {
  return (
    <>
      <Navbar />
      <HomeSlider />
      <CompanyValues />
      <ContactForm />
      <Footer />
    </>
  );
}

export default App;
