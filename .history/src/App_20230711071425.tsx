import { useState } from "react";
import Navbar from "./component/Navbar";
import HomeSlider from "./component/HomeSlider";

function App() {
  const [count, setCount] = useState(0);

  return (
    <>
      <Navbar />
      <HomeSlider />
    </>
  );
}

export default App;
