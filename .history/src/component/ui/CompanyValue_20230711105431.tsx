import { useEffect, useState } from "react";

type ValueCardProps = {
  title: string;
  value: string;
  icon: string;
  color: string;
  size?: string;
  triangle: boolean;
  triangleColor?: string;
};

const CompanyValue = ({
  title,
  value,
  icon,
  color,
  size,
  triangle,
  triangleColor,
}: ValueCardProps) => {
  const [width, setWidth] = useState<number>(0);

  useEffect(() => {
    setWidth(window.innerWidth);

    // add event listener to window
    window.addEventListener("resize", () => {
      setWidth(window.innerWidth);
    });

    // remove event listener
    return () => {
      window.removeEventListener("resize", () => {
        setWidth(window.innerWidth);
      });
    };
  }, []);

  return (
    <div
      style={{
        backgroundColor: color,
        border: "2px solid" + triangleColor,
      }}
      className="flex relative text-white w-80 flex-col items-center justify-center rounded-sm p-6"
    >
      <div
        style={{
          width: size,
        }}
        className="flex items-center justify-center  w-7 h-7"
      >
        <img src={icon} alt={title} width={48} />
      </div>
      <h2 className="text-xl xl:text-2xl font-bold mt-4">{title}</h2>
      <p className="text-center mt-2 text-sm xl:text-base">{value}</p>
      {triangle && (
        <div
          style={{
            borderLeftColor: width > 768 ? triangleColor : "transparent",
            borderTopColor: width <= 768 ? triangleColor : "transparent",
          }}
          className={`absolute -bottom-[20px] md:bottom-auto md:-right-[20px] md:top-1/2 md:-translate-y-1/2 md:border-y-[20px] border-x-[20px] md:border-x-0 border-x-transparent md:border-y-transparent border-t-[20px] md:border-l-[20px] border-y-transparent w-0 h-0]`}
        ></div>
      )}
    </div>
  );
};

export default CompanyValue;
