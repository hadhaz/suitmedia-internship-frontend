type ValueCardProps = {
  title: string;
  value: string;
  icon: string;
  color: string;
  size?: string;
};

const CompanyValue = ({ title, value, icon, color, size }: ValueCardProps) => {
  return (
    <div
      style={{
        backgroundColor: color,
      }}
      className="flex text-white w-80 flex-col items-center justify-center rounded-sm p-6"
    >
      <div
        style={{
          width: size,
        }}
        className="flex items-center justify-center  w-7 h-7"
      >
        <img src={icon} alt={title} width={48} />
      </div>
      <h2 className="text-2xl font-bold mt-4">{title}</h2>
      <p className="text-center  mt-2">{value}</p>
      <div className="border-t-[60px] border-b-[60px] w-0 h-0"></div>
    </div>
  );
};

export default CompanyValue;
