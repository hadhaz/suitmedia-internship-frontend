type ValueCardProps = {
  title: string;
  value: string;
  icon: string;
  color: string;
};

const ValueCard = ({ title, value, icon, color }: ValueCardProps) => {
  return (
    <div className="flex flex-col items-center justify-center bg-white rounded-xl p-6">
      <div className="flex items-center justify-center rounded-full bg-gray-100 w-16 h-16">
        <img src={icon} alt={title} width={48} />
      </div>
      <h2 className="text-2xl font-bold mt-4">{title}</h2>
      <p
        className="text-center text-gray-500 m
        t-2"
      >
        {value}
      </p>
    </div>
  );
};

export default ValueCard;
