const Title: React.FC = ({ children }) => {
    return <h1 className="text-center font-semibold text-xl lg:text-2xl xl:text-3xl py-5">{children}</h1>;
}

export default Title;