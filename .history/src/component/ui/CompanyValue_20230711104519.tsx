import { useEffect, useState } from "react";

type ValueCardProps = {
  title: string;
  value: string;
  icon: string;
  color: string;
  size?: string;
  triangle: boolean;
  triangleColor?: string;
};

const CompanyValue = ({
  title,
  value,
  icon,
  color,
  size,
  triangle,
  triangleColor,
}: ValueCardProps) => {
  const bgTriangleColor = `border-l-[${triangleColor}]`;
  // get width of screen
  const [width, setWidth] = useState<number>(0);

  useEffect(() => {
    setWidth(window.innerWidth);
  }, []);

  return (
    <div
      style={{
        backgroundColor: color,
        border: "2px solid" + triangleColor,
      }}
      className="flex relative text-white w-80 flex-col items-center justify-center rounded-sm p-6"
    >
      <div
        style={{
          width: size,
        }}
        className="flex items-center justify-center  w-7 h-7"
      >
        <img src={icon} alt={title} width={48} />
      </div>
      <h2 className="text-2xl font-bold mt-4">{title}</h2>
      <p className="text-center  mt-2">{value}</p>
      {triangle && (
        <div
          style={{
            borderLeftColor: width > 768 ? triangleColor : "transparent",
            borderTopColor: width <= 768 ? triangleColor : "transparent",
          }}
          className={`absolute -right-[20px] top-1/2 -translate-y-1/2 border-t-[20px] md:border-t-transparent border-b-[20px] md:border-b- border-l-[20px] border-y-transparent w-0 h-0]`}
        ></div>
      )}
    </div>
  );
};

export default CompanyValue;
