type ValueCardProps = {
  title: string;
  value: string;
  icon: string;
  color: string;
  size?: string;
};

const CompanyValue = ({ title, value, icon, color, size }: ValueCardProps) => {
  return (
    <div
      style={{
        backgroundColor: color,
      }}
      className="flex relative text-white w-80 flex-col items-center justify-center rounded-sm p-6"
    >
      <div
        style={{
          width: size,
        }}
        className="flex items-center justify-center  w-7 h-7"
      >
        <img src={icon} alt={title} width={48} />
      </div>
      <h2 className="text-2xl font-bold mt-4">{title}</h2>
      <p className="text-center  mt-2">{value}</p>
      <div className="absolute -right-[20px] top-1/2 -translate-y-1/2 border-t-[20px] border-b-[20px] border-l-[20px] border-l-inherit border-y-transparent w-0 h-0"></div>
    </div>
  );
};

export default CompanyValue;
