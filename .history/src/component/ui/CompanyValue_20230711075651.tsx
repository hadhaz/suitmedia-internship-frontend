type ValueCardProps = {
  title: string;
  value: string;
  icon: string;
  color: string;
};

const CompanyValue = ({ title, value, icon, color }: ValueCardProps) => {
  return (
    <div
      style={{
        backgroundColor: color,
      }}
      className="flex w-80 flex-col items-center justify-center rounded-xl p-6"
    >
      <div className="flex items-center justify-center rounded-full w-16 h-16">
        <img src={icon} alt={title} width={48} />
      </div>
      <h2 className="text-2xl font-bold mt-4">{title}</h2>
      <p
        className="text-center text-gray-500 m
        t-2"
      >
        {value}
      </p>
    </div>
  );
};

export default CompanyValue;
