import { useState } from "react";
import Title from "./ui/Title";
import { useFormik } from "formik";

const ContactForm: React.FC = () => {
  const [nameError, setNameError] = useState<string>();
  const [emailError, setEmailError] = useState<string>();
  const [messageError, setMessageError] = useState<string>();

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      message: "",
    },
    onSubmit: (values) => {
      if (values.name === "") {
        setNameError("The field is required");
      }

      // add regex for email validation
      const emailRegex = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/;
        if (values.email === "") {
            setEmailError("The field is required");
        } else if (!emailRegex.test(values.email)) {
            setEmailError("Invalid email format");
        }

        

      alert(JSON.stringify(values, null, 2));
    },
  });

  return (
    <div className="mb-36 mx-3">
      <Title>CONTACT US</Title>
      <form onSubmit={formik.handleSubmit}>
        <div className="flex max-w-2xl mx-auto flex-col gap-y-4">
          <div className="flex flex-col gap-y-2">
            <label htmlFor="name">Name</label>
            <input
              id="name"
              name="name"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.name}
              className="border-2 py-1 px-2 focus:border-blue-500 outline-none"
            />
          </div>
          <div className="flex flex-col gap-y-2">
            <label htmlFor="email">Email</label>
            <input
              id="email"
              name="email"
              type="email"
              onChange={formik.handleChange}
              value={formik.values.email}
              className="border-2 py-1 px-2 focus:border-blue-500 outline-none"
            />
          </div>
          <div className="flex flex-col gap-y-2">
            <label htmlFor="message">Message</label>
            <textarea
              id="message"
              name="message"
              onChange={formik.handleChange}
              value={formik.values.message}
              className="border-2 h-48 py-1 px-2 focus:border-blue-500 outline-none"
            />
          </div>
          <button type="submit" className="bg-blue-400 text-white py-2">
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default ContactForm;
