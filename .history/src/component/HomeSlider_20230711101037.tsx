import "keen-slider/keen-slider.min.css";
import { useKeenSlider } from "keen-slider/react";
import { useState } from "react";
import slider1 from "../assets/slider-1.jpg";
import slider2 from "../assets/slider-2.jpg";
import Arrow from "./ui/Arrow";

const HomeSlider: React.FC = () => {
  const [currentSlide, setCurrentSlide] = useState<number>(0);
  const [loaded, setLoaded] = useState<boolean>(false);
  const [sliderRef, instanceRef] = useKeenSlider<HTMLDivElement>({
    initial: 0,
    slideChanged(slider) {
      setCurrentSlide(slider.track.details.rel);
    },
    created() {
      setLoaded(true);
    },
  });

  return (
    <header className="relative">
      <div ref={sliderRef} className="keen-slider">
        <div className="keen-slider__slide h-96 md:h-fit">
          <h2>
            THIS IS A PLACE WHERE TECHNOLOGY & CREATIVITY FUSED INTO DIGITAL
            CHEMISTRY
          </h2>
          <img className="w-full h-full object-cover" src={slider1} />
        </div>
        <div className="keen-slider__slide h-96 md:h-fit">
          <img className="w-full h-full object-cover" src={slider2} />
          <h2>
            WE DON'T HAVE THE BEST BUT WE THE GREATEST TEAM
          </h2>
        </div>
      </div>

      {loaded && instanceRef.current && (
        <>
          <Arrow
            left
            onClick={(e: any) =>
              e.stopPropagation() || instanceRef.current?.prev()
            }
            disabled={currentSlide === 0}
          />

          <Arrow
            onClick={(e: any) =>
              e.stopPropagation() || instanceRef.current?.next()
            }
            disabled={
              currentSlide ===
              instanceRef.current.track.details.slides.length - 1
            }
          />
        </>
      )}
      {loaded && instanceRef.current && (
        <div className="flex absolute bottom-0 left-1/2 -translate-x-1/2 py-3 justify-center">
          {[
            ...Array(instanceRef.current.track.details.slides.length).keys(),
          ].map((idx) => {
            return (
              <button
                key={idx}
                onClick={() => {
                  instanceRef.current?.moveToIdx(idx);
                }}
                className={
                  "w-3 h-3 bg-gray-700 rounded-full mx-2 p-2 cursor-pointer" +
                  (currentSlide === idx ? " bg-slate-100" : "")
                }
              ></button>
            );
          })}
        </div>
      )}
    </header>
  );
};

export default HomeSlider;
