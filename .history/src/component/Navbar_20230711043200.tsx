const Navbar: React.FC = () => {
  return (
    <nav className="flex jus">
      <h1>Company</h1>
      <ul>
        <li>
          <a href="/">ABOUT</a>
        </li>
        <li>
          <a href="/about">OUR WORK</a>
        </li>
        <li>
          <a href="/our-team">OUR TEAM</a>
        </li>
        <li>
          <a href="/about">CONTACT</a>
        </li>
      </ul>
    </nav>
  );
};


export default Navbar;