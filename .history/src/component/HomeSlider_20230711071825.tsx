import "keen-slider/keen-slider.min.css";
import { useKeenSlider } from "keen-slider/react";
import { useState } from "react";
import slider1 

const HomeSlider: React.FC = () => {
  const [currentSlide, setCurrentSlide] = useState<number>(0);
  const [loaded, setLoaded] = useState<boolean>(false);
  const [sliderRef, instanceRef] = useKeenSlider<HTMLDivElement>({
    initial: 0,
    slideChanged(slider) {
      setCurrentSlide(slider.track.details.rel);
    },
    created() {
      setLoaded(true);
    },
  });

  return (
    <header>
      <div ref={sliderRef} className="keen-slider">
        <div className="keen-slider__slide">
          <img src={} />
        </div>
        <div className="keen-slider__slide">
          <img src={} />
        </div>
      </div>
    </header>
  );
};

export default HomeSlider;
