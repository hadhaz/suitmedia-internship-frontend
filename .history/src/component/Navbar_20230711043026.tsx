const Navbar: React.FC = () => {
  return (
    <nav>
      <h1>Navbar</h1>
      <ul>
        <li>
          <a href="/">Home</a>
        </li>
        <li>
          <a href="/about">About</a>
        </li>
      </ul>
    </nav>
  );
};


export default Navbar;