// define function component with type of FC
const Navbar: React.FC = () => {
  // return JSX
  return (
    <nav>
      <h1>Navbar</h1>
      <ul>
        <li>
          <a href="/">Home</a>
        </li>
        <li>
          <a href="/about">About</a>
        </li>
      </ul>
    </nav>
  );
};


export default Navbar;