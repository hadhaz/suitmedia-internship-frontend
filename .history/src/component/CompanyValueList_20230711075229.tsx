import CompanyValue from "./ui/CompanyValue";

const CompanyValues: React.FC = () => {
  return (
    <main className="mt-20">
      <h1 className="text-center font-semibold text-lg lg:text-xl xl:text-2xl">
        OUR VALUES
      </h1>
      <div>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Non quibusdam, maiores corrupti labore facilis odio quas amet officiis, sed adipisci accusamus.
        <CompanyValue color="red-400" icon="" title="INNOVATIVE" value="lorem20" />
      </div>
    </main>
  );
};

export default CompanyValues;
