import Title from "./ui/Title";
import { useFormik } from "formik";

const ContactForm: React.FC = () => {
  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      message: "",
    },
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  return (
    <div>
      <Title>CONTACT US</Title>
      <form onSubmit={formik.handleSubmit}></form>
    </div>
  );
};

export default ContactForm;
