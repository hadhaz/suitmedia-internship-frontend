import Title from "./ui/Title";
import { useFormik } from "formik";

const ContactForm: React.FC = () => {
  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      message: "",
    },
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  return (
    <div>
      <Title>CONTACT US</Title>
        <form onSubmit={formik.handleSubmit}>
            <div className="flex flex-col gap-y-4">
                <input
                    id="name"
                    name="name"
                    type="text"
                    placeholder="Name"
                    onChange={formik.handleChange}
                    value={formik.values.name}
                    className="border border-gray-400 p-2 rounded-md"
                />
                <input
                
    </div>
  );
};

export default ContactForm;
