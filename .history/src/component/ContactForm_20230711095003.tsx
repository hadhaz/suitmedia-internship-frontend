import Title from "./ui/Title";
import { useFormik } from "formik";

const ContactForm: React.FC = () => {
  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      message: "",
    },
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  return (
    <div>
      <Title>CONTACT US</Title>
      <form onSubmit={formik.handleSubmit}>
        <div className="flex flex-col gap-y-4">
            <div className="flex flex-col gap-y-2">
                <label htmlFor="name">Name</label>
                <input
                    id="name"
                    name="name"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.name}
                />
            </div>
            <div className="flex flex-col gap-y-2">
                <label htmlFor="email">Email</label>
                <input

                    id="email"
                    name="email"
                    type="email"
                    onChange={formik.handleChange}
                    value={formik.values.email}
                />
            </div>
            
      </form>
    </div>
  );
};

export default ContactForm;
