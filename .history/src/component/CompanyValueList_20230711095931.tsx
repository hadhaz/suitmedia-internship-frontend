import { companyValues } from "../data/company-values";
import CompanyValue from "./ui/CompanyValue";
import Title from "./ui/Title";

const CompanyValues: React.FC = () => {
  return (
    <main className="mt-14 mb-28">
      <Title>OUR VALUES</Title>
      <div className="flex justify-center gap-x-8 2xl:gap-x-12">
        {companyValues.map((value, index) => (
          <CompanyValue key={index} {...value} />
        ))}
      </div>
    </main>
  );
};

export default CompanyValues;
