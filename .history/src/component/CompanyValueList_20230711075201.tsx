import CompanyValue from "./ui/CompanyValue";

const CompanyValues: React.FC = () => {
  return (
    <main className="mt-20">
      <h1 className="text-center font-semibold text-lg lg:text-xl xl:text-2xl">
        OUR VALUES
      </h1>
      <div>
        <CompanyValue color="red-400"  />
      </div>
    </main>
  );
};

export default CompanyValues;
