const Navbar: React.FC = () => {
  return (
    <nav>
      <h1>Company</h1>
      <ul>
        <li>
          <a href="/">About</a>
        </li>
        <li>
          <a href="/about">OUR WORK</a>
        </li>
        <li>
          <a href="/our-team">OUR TEAM</a>
        </li>
        <li>
          <a href="/about">CONTACT</a>
        </li>
      </ul>
    </nav>
  );
};


export default Navbar;