import iconHamburgerPath from "../assets/icon-hamburger.svg";
import AboutMenu from "./AboutMenu";

const Navbar: React.FC = () => {
  return (
    <nav className="flex justify-between items-center font-medium px-10 md:px-20 lg:px-36 xl:px-36 2xl:px-52 py-4 xl:py-5">
      <h1 className="text-2xl xl:text-3xl 2xl:text-4xl">Company</h1>
      <ul className="hidden md:flex gap-x-6 xl:text-lg 2xl:text-xl text-slate-500">
        <li className="px-3">
          <AboutMenu />
        </li>
        <li className="px-3">
          <a href="/about">OUR WORK</a>
        </li>
        <li className="px-3">
          <a href="/our-team">OUR TEAM</a>
        </li>
        <li className="px-3">
          <a href="/about">CONTACT</a>
        </li>
      </ul>
      <button className="md:hidden">
        <img src={iconHamburgerPath} alt="hamburger" width={48} />
      </button>
    </nav>
  );
};

export default Navbar;
