import CompanyValue from "./ui/CompanyValue";

const CompanyValues: React.FC = () => {
  return (
    <main className="mt-16">
      <h1 className="text-center font-semibold text-xl lg:text-2xl xl:text-3xl py-5">
        OUR VALUES
      </h1>
      <div className="flex justify-center gap-x-8">
        {companyValues.map((value, index) => (
          <CompanyValue key={index} {...value} />
        ))}
      </div>
    </main>
  );
};

export default CompanyValues;
