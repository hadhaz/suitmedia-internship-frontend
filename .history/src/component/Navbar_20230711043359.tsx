const Navbar: React.FC = () => {
  return (
    <nav className="flex justify-between px-6 md:px-24">
      <h1>Company</h1>
      <ul className="md:flex gap-x-6 hidden">
        <li>
          <a href="/">ABOUT</a>
        </li>
        <li>
          <a href="/about">OUR WORK</a>
        </li>
        <li>
          <a href="/our-team">OUR TEAM</a>
        </li>
        <li>
          <a href="/about">CONTACT</a>
        </li>
      </ul>
    </nav>
  );
};


export default Navbar;