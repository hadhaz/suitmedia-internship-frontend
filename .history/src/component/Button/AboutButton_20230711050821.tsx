import { useState } from "react";

const AboutButton: React.FC = () => {
  const [open, setOpen] = useState<Boolean>(false);

  return (
    <button
      onMouseOver={() => setOpen(true)}
      onMouseLeave={() => setOpen(false)}
    >
      <a href="/">ABOUT</a>
    </button>
  );
};

export default AboutButton;
