import { useState } from "react";

const AboutButton: React.FC = () => {
  const [open, setOpen] = useState<Boolean>(false);

  return (
    <div>
      <button
        onMouseOver={() => setOpen(true)}
        
      >
        <a href="/">ABOUT</a>
      </button>
    </div>
  );
};

export default AboutButton;
