import { companyValues } from "../data/company-values";
import CompanyValue from "./ui/CompanyValue";

const CompanyValues: React.FC = () => {
  return (
    <main className="mt-14 mb-28">
      <ti className="text-center font-semibold text-xl lg:text-2xl xl:text-3xl py-5">
        OUR VALUES
      </ti>
      <div className="flex justify-center gap-x-8">
        {companyValues.map((value, index) => (
          <CompanyValue key={index} {...value} />
        ))}
      </div>
    </main>
  );
};

export default CompanyValues;
