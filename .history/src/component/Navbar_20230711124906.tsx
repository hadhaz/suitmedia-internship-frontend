import { useState } from "react";
import iconHamburgerPath from "../assets/icon-hamburger.svg";
import AboutMenu from "./AboutMenu";

const Navbar: React.FC = () => {
  const [open, setOpen] = useState<Boolean>(false);
  const [aboutOpen, setAboutOpen] = useState<Boolean>(false);
  const toggleMenu = () => setOpen((prev) => !prev);
  const toggleAbout = () => setAboutOpen((prev) => !prev);

  return (
    <nav className="flex w-full relative justify-between items-center font-medium px-6 md:px-20 lg:px-28 xl:px-36 2xl:px-52">
      <h1 className="text-2xl xl:text-3xl 2xl:text-4xl">Company</h1>
      <ul className="hidden md:flex xl:text-lg 2xl:text-xl text-slate-500">
        <li className="hover:bg-[#] duration-500">
          <AboutMenu />
        </li>
        <li className="px-3 py-3 xl:py-4 2xl:py-5 hover:bg-[#] duration-500">
          <a href="/our-work">OUR WORK</a>
        </li>
        <li className="px-3 py-3 xl:py-4 2xl:py-5 hover:bg-[#] duration-500">
          <a href="/our-team">OUR TEAM</a>
        </li>
        <li className="px-3 py-3 xl:py-4 2xl:py-5 hover:bg-[#] duration-500">
          <a href="/contact">CONTACT</a>
        </li>
      </ul>
      {open && (
        <ul className="flex flex-col py-6 md:hidden absolute bottom-0 translate-y-full bg-white w-full left-0 z-20">
          <li className="px-3 text-center py-4">
            <button onClick={toggleAbout}>ABOUT</button>
          </li>
          {aboutOpen && (
            <>
              <li className="px-3 text-center py-2 bg-gray-100 mb-1 text-sm mx-6">
                <a href="/">HISTORY</a>
              </li>
              <li  className="px-3 text-center py-2 bg-gray-100 text-sm mx-6">
                <a href="/">VISION MISION</a>
              </li>
            </>
          )}
          <li className="px-3 py-4 text-center">
            <a href="/our-work">OUR WORK</a>
          </li>
          <li className="px-3 py-4 text-center">
            <a href="/our-team">OUR TEAM</a>
          </li>
          <li className="px-3 py-4 text-center">
            <a href="/contact">CONTACT</a>
          </li>
        </ul>
      )}
      <button onClick={toggleMenu} className="md:hidden">
        <img src={iconHamburgerPath} alt="hamburger" width={48} />
      </button>
    </nav>
  );
};

export default Navbar;
