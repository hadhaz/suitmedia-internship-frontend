import CompanyValue from "./ui/CompanyValue";

const lorem =
  "lorem ipsum dolor sit amet, consectetur adipiscing elit. Maxime exercitationem dolorem deserunt, unde, eaque Ipsa?";

type CompanyValues = {
  color: string;
  icon: string;
  title: string;
  value: string;
}[];

const companyValues: CompanyValues = [
  {
    color: "#EB7A70",
    icon: "",
    title: "INNOVATIVE",
    value:
      "lorem ipsum dolor sit amet, consectetur adipiscing elit. Maxime exercitationem dolorem deserunt, unde, eaque Ipsa?",
  },
  {
    color: "#689970",
    icon: "",
    title: "LOYALTY",
    value: ,
  },
  {
    color: "#5497C3",
    icon: "",
    title: "RESPECT",
    value: lorem,
  },
];

const CompanyValues: React.FC = () => {
  return (
    <main className="mt-16">
      <h1 className="text-center font-semibold text-xl lg:text-2xl xl:text-3xl py-5">
        OUR VALUES
      </h1>
      <div className="flex justify-center gap-x-8">
        {companyValues.map((value, index) => (
          <CompanyValue key={index} {...value} />
        ))}
      </div>
    </main>
  );
};

export default CompanyValues;
