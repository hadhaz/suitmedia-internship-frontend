import { useState } from "react";

const AboutMenu: React.FC = () => {
  const [open, setOpen] = useState<Boolean>(false);

  return (
    <div onMouseLeave={() => setOpen(false)} className="relative">
      <button onMouseOver={() => setOpen(true)}>
        <a href="/">ABOUT</a>
      </button>
      <ul
        className={`absolute left-0 w-40 pt-2 shadow-sm ${
          open ? "block" : "hidden"
        }`}
      >
        <li className="flex w-full items-center px-3 py-2 text-sm hover:bg-gray-500 hover:text-white cursor-pointer">
          HISTORY
        </li>
        <li className="flex w-full items-center px-3 py-2 text-sm hover:bg-gray-500 hover:text-white cursor-pointer">
          VIS
        </li>
        <li className="flex w-full items-center px-3 py-2 text-sm hover:bg-gray-500 hover:text-white cursor-pointer">
          Dropdown List 3
        </li>
      </ul>
    </div>
  );
};

export default AboutMenu;
