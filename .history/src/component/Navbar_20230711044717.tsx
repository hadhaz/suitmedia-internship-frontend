import iconHamburgerPath from "../assets/icon-hamburger.svg";

const Navbar: React.FC = () => {
  return (
    <nav className="flex justify-between items-center px-6 md:px-24  py-4">
      <h1 className="font-medium text-2xl">Company</h1>
      <ul className="hidden md:flex gap-x-6">
        <li>
          <a href="/">ABOUT</a>
        </li>
        <li>
          <a href="/about">OUR WORK</a>
        </li>
        <li>
          <a href="/our-team">OUR TEAM</a>
        </li>
        <li>
          <a href="/about">CONTACT</a>
        </li>
      </ul>
      <button className="md:hidden">
        <img src={iconHamburgerPath} alt="hamburger" width={48} />
      </button>
    </nav>
  );
};

export default Navbar;
