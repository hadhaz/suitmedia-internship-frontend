import { useState } from "react";

const AboutMenu: React.FC = () => {
  const [open, setOpen] = useState<Boolean>(false);

  return (
    <div onMouseLeave={() => setOpen(false)} className="relative px-3 z-10">
      <button onMouseOver={() => setOpen(true)} className="py-3 xl:py-4 2xl:py-5">
        <a href="/">ABOUT</a>
      </button>
      <ul
        className={`absolute left-0 w-40 z-10 bg-white shadow-sm ${
          open ? "block" : "hidden"
        }`}
      >
        <li className="flex w-full items-center p-3 text-sm duration-500 hover:bg-[] hover:text-white cursor-pointer">
          HISTORY
        </li>
        <li className="flex w-full items-center p-3 text-sm duration-500 hover:bg-[] hover:text-white cursor-pointer">
          VISION MISION
        </li>
      </ul>
    </div>
  );
};

export default AboutMenu;
