import CompanyValue from "./ui/CompanyValue";

const lorem =
  "lorem ipsum dolor sit amet, consectetur adipiscing elit. Maxime exercitationem dolorem deserunt, unde, eaque Ipsa?";

type CompanyValues = {
  color: string;
  icon: string;
  title: string;
  value: string;
}[];

const companyValues: CompanyValues = [
  {
    color: "#EB7A70",
    icon: "",
    title: "INNOVATIVE",
    value: lorem,
  },
  {
    color: "#689970",
    icon: "",
    title: "LOYALTY",
    value: lorem,
  },
  {
    color: "#5497C3",
    icon: "",
    title: "RESPECT",
    value: lorem,
  },
];

const CompanyValues: React.FC = () => {
  return (
    <main className="mt-20">
      <h1 className="text-center font-semibold text-lg lg:text-xl xl:text-2xl">
        OUR VALUES
      </h1>
      <div className="flex justify-center gap-x-8">
        {companyValues.map((value, index) => (
          <CompanyValue key={index} {...value} />
        ))}
      </div>
    </main>
  );
};

export default CompanyValues;
