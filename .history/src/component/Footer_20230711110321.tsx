import logoFacebook from '../assets/facebook.png';

const Footer: React.FC = () => {
  return (
    <footer>
      <h3>Copyright @ 2016. PT COMPANY</h3>
      <div>
        <a href="facebook.com">
            <img src="" alt="" />
        </a>
      </div>
    </footer>
  );
};

export default Footer;
