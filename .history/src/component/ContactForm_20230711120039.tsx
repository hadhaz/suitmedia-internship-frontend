import Title from "./ui/Title";
import { useFormik } from "formik";
import * as Yup from "yup";

const ContactForm: React.FC = () => {
  const Schema = Yup.object().shape({
    name: Yup.string().required("The field is required"),
    email: Yup.string()
      .email("Invalid email address")
      .required("The field is required"),
    message: Yup.string().required("The field is required"),
  });

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      message: "",
    },
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
    validationSchema: Schema,
  });

  // focus on the first invalid field
  

  return (
    <div className="mb-36 mx-3">
      <Title>CONTACT US</Title>
      <form onSubmit={formik.handleSubmit}>
        <div className="flex max-w-2xl mx-auto flex-col gap-y-4">
          <div className="flex flex-col gap-y-2">
            <label htmlFor="name">Name</label>
            <input
              id="name"
              name="name"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.name}
              className={`border-2 py-1 px-2 focus:border-blue-500 outline-none ${
                formik.errors.name && formik.touched.name
                  ? "border-red-500"
                  : "border-gray-300"
              }`}
            />
            {formik.errors.name && formik.touched.name ? (
              <div className="text-red-500">{formik.errors.name}</div>
            ) : null}
          </div>
          <div className="flex flex-col gap-y-2">
            <label htmlFor="email">Email</label>
            <input
              id="email"
              name="email"
              type="email"
              onChange={formik.handleChange}
              value={formik.values.email}
              className={`border-2 py-1 px-2 focus:border-blue-500 outline-none ${
                formik.errors.email && formik.touched.email
                  ? "border-red-500"
                  : "border-gray-300"
              }`}
            />
            {formik.errors.email && formik.touched.email ? (
              <div className="text-red-500">{formik.errors.email}</div>
            ) : null}
          </div>
          <div className="flex flex-col gap-y-2">
            <label htmlFor="message">Message</label>
            <textarea
              id="message"
              name="message"
              onChange={formik.handleChange}
              value={formik.values.message}
              className={`border-2 h-48 py-1 px-2 focus:border-blue-500 outline-none ${
                formik.errors.message && formik.touched.message
                  ? "border-red-500"
                  : "border-gray-300"
              }`}
            />
            {formik.errors.message && formik.touched.message ? (
              <div className="text-red-500">{formik.errors.message}</div>
            ) : null}
          </div>
          <button type="submit" className="bg-blue-400 text-white py-2">
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default ContactForm;
