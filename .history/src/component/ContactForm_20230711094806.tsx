import Title from "./ui/Title";
import {useFormik} from "formik"

const ContactForm: React.FC = () => {
const formik  = useFormik({
    initialValues: {
        name: "",
        email: "",
        message: ""
    },
    

  return (
    <div>
      <Title>CONTACT US</Title>
    </div>
  );
};

export default ContactForm;
