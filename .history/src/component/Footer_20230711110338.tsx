import logoFacebook from "../assets/facebook.png";
import logoTwitter from "../assets/twitter.png";

const Footer: React.FC = () => {
  return (
    <footer>
      <h3>Copyright @ 2016. PT COMPANY</h3>
      <div>
        <a href="facebook.com">
          <img src={logoFacebook} alt="" />
        </a>
      </div>
    </footer>
  );
};

export default Footer;
