import logoFacebook from "../assets/facebook.png";
import logoTwitter from "../assets/twitter.png";

const Footer: React.FC = () => {
  return (
    <footer className="bg-[#333333]">
      <h3>Copyright @ 2016. PT COMPANY</h3>
      <div className="flex gap-x-8">
        <a href="facebook.com">
          <img src={logoFacebook} width={36} alt="fb" />
        </a>
        <a href="twitter.com">
          <img src={logoTwitter} width={36} alt="twitter" />
        </a>
      </div>
    </footer>
  );
};

export default Footer;
