import Title from "./ui/Title";
import { useFormik } from "formik";

const ContactForm: React.FC = () => {
  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      message: "",
    },
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  return (
    <div>
      <Title>CONTACT US</Title>
      <form onSubmit={formik.handleSubmit}>
        <div className="flex flex-col gap-y-4">
            
      </form>
    </div>
  );
};

export default ContactForm;
