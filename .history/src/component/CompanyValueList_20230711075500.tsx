import CompanyValue from "./ui/CompanyValue";

const lorem =
  "Lorem ipsum dolor sit amet consectetur adipisicing elit. Non quibusdam, maiores corrupti labore facilis odio quas amet officiis, sed adipisci accusamus.";

const CompanyValues: React.FC = () => {
  return (
    <main className="mt-20">
      <h1 className="text-center font-semibold text-lg lg:text-xl xl:text-2xl">
        OUR VALUES
      </h1>
      <div>
        <CompanyValue
          color="#"
          icon=""
          title="INNOVATIVE"
          value={lorem}
        />
      </div>
    </main>
  );
};

export default CompanyValues;
