import CompanyValue from "./ui/CompanyValue";

const lorem = "lorem ipsum dolor sit amet, consectetur adipiscing elit. Max"

const CompanyValues: React.FC = () => {
  return (
    <main className="mt-20">
      <h1 className="text-center font-semibold text-lg lg:text-xl xl:text-2xl">
        OUR VALUES
      </h1>
      <div className="flex justify-center gap-x-8">
        <CompanyValue
          color="#EB7A70"
          icon=""
          title="INNOVATIVE"
          value={lorem}
        />
        <CompanyValue
          color="#EB7A70"
          icon=""
          title="INNOVATIVE"
          value={lorem}
        />
        <CompanyValue
          color="#EB7A70"
          icon=""
          title="INNOVATIVE"
          value={lorem}
        />
      </div>
    </main>
  );
};

export default CompanyValues;
