import iconHamburgerPath from "../assets/icon-hamburger.svg";
import AboutMenu from "./AboutMenu";

const Navbar: React.FC = () => {
  return (
    <nav className="flex w-full justify-between items-center font-medium px-6 md:px-20 lg:px-28 xl:px-36 2xl:px-52">
      <h1 className="text-2xl xl:text-3xl 2xl:text-4xl">Company</h1>
      <ul className="hidden md:flex xl:text-lg 2xl:text-xl text-slate-500">
        <li className="hover:bg-gray-200 duration-500">
          <AboutMenu />
        </li>
        <li className="px-3 py-3 xl:py-4 2xl:py-5 hover:bg-gray-200 duration-500">
          <a href="/about">OUR WORK</a>
        </li>
        <li className="px-3 py-3 xl:py-4 2xl:py-5 hover:bg-gray-200 duration-500">
          <a href="/our-team">OUR TEAM</a>
        </li>
        <li className="px-3 py-3 xl:py-4 2xl:py-5 hover:bg-gray-200 duration-500">
          <a href="/about">CONTACT</a>
        </li>
      </ul>
      <ul className="">

      </ul>
      <button className="md:hidden">
        <img src={iconHamburgerPath} alt="hamburger" width={48} />
      </button>
    </nav>
  );
};

export default Navbar;
