import 

type CompanyValues = {
  color: string;
  icon: string;
  title: string;
  value: string;
}[];

export const companyValues: CompanyValues = [
  {
    color: "#EB7A70",
    icon: "",
    title: "INNOVATIVE",
    value:
      "lorem ipsum dolor sit amet, consectetur adipiscing elit. Maxime exercitationem dolorem deserunt, unde, eaque Ipsa?",
  },
  {
    color: "#689970",
    icon: "",
    title: "LOYALTY",
    value:
      "lorem ipsum dolor sit amet, consectetur adipiscing elit. Impedit similique, eum itaque facere temporibus dolores.",
  },
  {
    color: "#5497C3",
    icon: "",
    title: "RESPECT",
    value:
      "lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequuntur, sit? Tenetur at neque quod incidunt!",
  },
];
