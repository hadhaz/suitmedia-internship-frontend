import innovative from "../assets/innovative.png";
import loyalty from "../assets/loyalty.png";
import respect from "../assets/respect.png";

type CompanyValues = {
  color: string;
  icon: string;
  title: string;
  value: string;
  size?: string | undefined;
  triangle: boolean;
  triangleColor: string;
}[];

export const companyValues: CompanyValues = [
  {
    color: "#EB7A70",
    icon: innovative,
    title: "INNOVATIVE",
    size: "20px",
    triangle: true,
    triangleColor: "#E74E42",
    value:
      "lorem ipsum dolor sit amet, consectetur adipiscing elit. Maxime exercitationem dolorem deserunt, unde, eaque Ipsa?",
  },
  {
    color: "#689970",
    icon: loyalty,
    title: "LOYALTY",
    triangle: true,
    triangleColor: "#547D5B",
    value:
      "lorem ipsum dolor sit amet, consectetur adipiscing elit. Impedit similique, eum itaque facere temporibus dolores.",
  },
  {
    color: "#5497C3",
    icon: respect,
    title: "RESPECT",
    triangle: false,
    triangleColor: "",
    value:
      "lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequuntur, sit? Tenetur at neque quod incidunt!",
  },
];
