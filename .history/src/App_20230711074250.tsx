import { useState } from "react";
import Navbar from "./component/Navbar";
import HomeSlider from "./component/HomeSlider";
import CompanyValues from "./component/CompanyValues";

function App() {
  return (
    <>
      <Navbar />
      <HomeSlider />
      <CompanyValues />
    </>
  );
}

export default App;
