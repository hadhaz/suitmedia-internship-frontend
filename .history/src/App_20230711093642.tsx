import Navbar from "./component/Navbar";
import HomeSlider from "./component/HomeSlider";
import CompanyValues from "./component/CompanyValueList";
import ContactForm from "./component/ContactForm";

function App() {
  return (
    <>
      <Navbar />
      <HomeSlider />
      <CompanyValues />
      <ContactForm />
    </>
  );
}

export default App;
