import Navbar from "./component/Navbar";
import HomeSlider from "./component/HomeSlider";
import CompanyValues from "./component/CompanyValueList";

function App() {
  return (
    <>
      <Navbar />
      <HomeSlider />
      <CompanyValues />
    </>
  );
}

export default App;
